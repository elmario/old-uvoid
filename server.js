'use strict';

var http = require('http');
var flash = require('connect-flash');
var validator = require('express-validator');
var gzippo = require('gzippo');
var express = require('express');
var i18n = require('i18n');
var mailer = require('nodemailer');
var RedisStore = require('connect-redis')(express)
var sessionStore = new RedisStore();
var fs = require('fs');
var local = require('./middleware/local').local;
var auth  = require('./middleware/auth').auth;
var config = require('./config').config;
var common = require('./common').common;
var bitcoin = require('bitcoin');
var db_uvoid = common.db.connect('uvoid');

var Server = function() {
    this._rstore  = require('socket.io/lib/stores/redis');
    this._server  = express();
};

Server.prototype = {
    init: function(params) {
        this.params = params || {};
        this.setupServer();
        this.startServer();
        this.initRoutes();
    },

    setupServer: function() {
        var _this  = this;
        var params = this.params;

        i18n.configure({
            locales: ['en', 'cn', 'rs', 'es', 'fr', 'ar', 'hi', 'de'],
            cookie: 'uvl',
            directory: __dirname + '/locales'
        });

        this._bitcoin = new bitcoin.Client({
            host: 'localhost',
            port: 8332,
            user: '493829299934',
            pass: 'kjk2jdff2ijwe32esdfskqmc23rnek',
            timeout: 5000
        });

        this._server.configure(function() {
            this.set('port', params.port);
            this.set('views', config.client.express.viewFolder);
            this.set('view engine', config.client.express.viewEngine);

            this.use(express.cookieParser());
            this.use(express.favicon(config.client.express.favicon));
            this.use(express.logger('dev'));
            this.use(flash());
            this.use(express.bodyParser());
            this.use(validator);
            this.use(express.methodOverride());
            this.use(express.cookieParser(config.keys.cookie));
            this.use(i18n.init);
            this.use(
                express.session ({
                    key: 'uvoid',
                    secret: config.keys.session,
                    store: sessionStore
                })
            );

            this.use(this.router);
            this.use(gzippo.staticGzip(config.client.express.publicFolder));
        });

        this._server.configure( 'development', function() {
            this.use(express.errorHandler());
        });
    },

    startServer: function() {
        var _this = this;
        this.client = http.createServer(this._server).listen(this._server.get('port'), function() {
            console.log('\nUVOID server instance started [' + _this._server.get('port') + ']');
        });
    },

    initRoutes: function() {
        var _this = this;
        this._server.get('/', function(req, res, next) {
            if(!req.session.account) {
                res.redirect('/login');
            } else {
                res.redirect('/trade');
            }
        });

        this._server.get('/register', [local], function(req, res, next) {
            if(!req.session.randomUsername) {
                req.session.randomUsername = common.createUsername();
            }
            res.render('register', {
                title: 'Register',
                randomUsername: req.session.randomUsername
            });
        });

        this._server.post('/register', [local], function(req, res, next) {
            req.assert('registerPassword', 'Please enter a password (min 5 character)').notEmpty().len(5, 50);
            req.assert('registerConfirmPassword', 'Please enter a confirmation password (min 5 characters)').notEmpty(5, 50);

            var errors = req.validationErrors();
            if(!errors) {
                var queryString = "SELECT accountId FROM accounts WHERE username = ?";
                var queryValues = [req.session.randomUsername];

                db_uvoid.query(queryString, queryValues, function(err, result) {
                    if(err) {
                        console.log(err);
                    }

                    if(result.length > 0) {
                        req.flash('errorMessage', 'That username is already taken, a new one has been generated');
                        req.session.randomUsername = common.createUsername();
                        res.redirect('/register');
                    } else {
                        var queryString = "INSERT INTO accounts(username, password) VALUES(?, ?)";
                        var queryValues = [
                            req.session.randomUsername,
                            common.createSha512Hash(req.body.registerPassword)
                        ];
                        db_uvoid.query(queryString, queryValues, function(err, result) {
                            if(err) {
                                console.log(err);
                            }
                            req.flash('successMessage', 'Your account has been created!');
                            res.redirect('/login');
                        })
                    }
                });
            } else {    
                req.flash('errorMessage', errors[0].msg);
                res.redirect('/register');
            }            
        });

        this._server.get('/terms', [local], function(req, res, next) {
            res.render('terms', {
                title: 'Terms'
            });
        });

        this._server.get('/trade', [local, auth], function(req, res, next) {
            var queryString = "SELECT * FROM listings LEFT JOIN accounts ON accounts.accountId = listings.seller LEFT JOIN categories ON categories.categoryId = listings.category ORDER BY timestamp";
            var queryValues = [];

            db_uvoid.query(queryString, queryValues, function(err, result) {
                if(err) {
                    console.log(err);
                }
                res.render('index/trade', {
                    title: 'Trade',
                    listings: result
                });
            });
        });

        this._server.get('/trade/category/:categoryId', [local, auth], function(req, res, next) {
            var queryString = "SELECT * FROM listings LEFT JOIN accounts ON accounts.accountId = listings.seller LEFT JOIN categories ON categories.categoryId = listings.category WHERE listings.category = ? ORDER BY timestamp"
            var queryValues = [
                req.params.categoryId
            ];

            db_uvoid.query(queryString, queryValues, function(err, result) {
                if(err) {
                    console.log(err);
                }
                res.render('index/trade', {
                    title: 'Trade',
                    listings: result
                });
            });
        });

        this._server.get('/trade/view/:listingId', [local, auth], function(req, res, next) {
            _this.getListing(req.params.listingId, function(listingData) {
                if(listingData) {
                    res.render('index/view-listing', {
                        title: 'View Listing',
                        listing: listingData
                    });
                } else {
                    req.flash('errorMessage', 'Listing not found');
                    res.redirect('/trade');
                }
            });
        });

        this._server.get('/trade/start/:listingId', [local], function(req, res, next) {
            _this.getListing(req.params.listingId, function(listing) {
                if(listing.price === 0) {
                    res.json('Address generation not required');
                } else {
                    _this.getTransaction(listing.listingId, req.session.account.accountId, function(tx) {
                        if(typeof tx !== 'undefined') {
                            res.json(tx.tempAddress);
                        } else {
                            _this._bitcoin.getNewAddress('TRANSACTION', function(err, address) {
                                if(err) {
                                    console.log(err);
                                }
                                var queryString = "INSERT INTO transactions(status, tempAddress, listingId, buyer) VALUES('IN-PROGRESS', ? , ?, ?)";
                                var queryValues = [
                                    address,
                                    req.params.listingId,
                                    req.session.account.accountId
                                ];
                                db_uvoid.query(queryString, queryValues, function(err, result) {
                                    if(err) {
                                        console.log(err);
                                    }
                                    res.json(address);
                                });
                            });
                        }
                    });
                }
            });
        });

        this._server.get('/trade/check/:listingId', [local], function(req, res, next) {
            _this.getListing(req.params.listingId, function(listing) {
                if(listing) {
                    if(listing.price ===  0) {
                        res.json(true);
                    } else {
                        _this.checkTransaction(listing, req.session.account.accountId, function(result) {
                            res.json(result);
                        });
                    }
                }
            });
        });

        this._server.get('/messages', [local, auth], function(req, res, next) {
            var queryString = "SELECT * FROM messages LEFT JOIN accounts ON accounts.accountId = messages.toAccount WHERE toAccount = ? ORDER BY messageId DESC";
            var queryValues = [
                req.session.account.accountId
            ];
            
            db_uvoid.query(queryString, queryValues, function(err, result) {
                if(err) {
                    console.log(err);
                }

                var queryString = "UPDATE messages SET status = 1 WHERE toAccount = ?";
                var queryValues = [
                    req.session.account.accountId
                ];

                db_uvoid.query(queryString, queryValues, function(err, result2) {
                    if(err) {
                        console.log(err);
                    }
                    res.render('index/messages', {
                        title: 'Messages',
                        messages: result
                    });
                });
            });
        });

        this._server.get('/messages/unreadCount', [local, auth], function(req, res, next) {
            var queryString = 'SELECT COUNT(*) AS unreadCount FROM messages WHERE toAccount = ? AND status = 0';
            var queryValues = [
                req.session.account.accountId
            ];
            db_uvoid.query(queryString, queryValues, function(err, result) {
                if(err) {
                    console.log(err);
                }
                res.json(result[0].unreadCount);
            });
        });

        this._server.post('/messages/init', [local, auth], function(req, res, next) {
            _this.getListing(req.body.initMessageId, function(listing) {
                if(listing) {
                    req.sanitize('initMessageId').xss();
                    _this.getListing(req.body.initMessageId, function(listing) {
                        if(listing) {
                            var messageBody = req.session.account.username + ' has sent a purchase message regarding your listing "' + listing.title + '" ' + req.body.initMessageBody;
                            _this.sendMessage(req.session.account.accountId, listing.seller, messageBody, function() {
                                req.flash('successMessage', 'Your message has been sent');
                                res.redirect('/trade');
                            });
                        }
                    });
                }
            });
        });

        this._server.get('/messages/reply/:messageId', [local, auth], function(req, res, next) {
            _this.getMessage(req.params.messageId, function(message) {
                if(message && message.toAccount === req.session.account.accountId) {
                    res.render('index/messages-reply', {
                        title: 'Reply'
                    });
                } else {
                    req.flash('errorMessage', 'Message not found');
                    res.redirect('/messages');
                }
            });
        });

        this._server.get('/messages/delete/:messageId', [local, auth], function(req, res, next) {
            _this.getMessage(req.params.messageId, function(message) {
                if(message && message.toAccount === req.session.accoun.accountId) {
                    var queryString = "DELETE FROM messages WHERE messageId = ? AND toAccount = ?";
                    var queryValues = [
                        req.params.messageId,
                        req.session.account.accountId
                    ];

                    db_uvoid.query(queryString, queryValues, function(err, result) {
                        if(err) {
                            console.log(err);
                        }

                        req.flash('successMessage', 'Message deleted');
                        res.redirect('/messages');
                    });
                }
            });
        });     

        this._server.post('/messages/reply/:messageId', [local, auth], function(req, res, next) {
            req.assert('replyBody', 'Please enter a reply body. 5-1000 characters').notEmpty().len(5, 1000);
            req.sanitize('replyBody').xss();

            var errors = req.validationErrors();
            if(!errors) {
                _this.getMessage(req.params.messageId, function(message) {
                    if(message && message.toAccount === req.session.accoun.accountId) {
                        _this.sendMessage(req.session.account.accountId, message.fromAccount, req.body.replyBody, function() {
                            req.flash('successMessage', 'Your message has been sent');
                            res.redirect('/messages');
                        });
                    } else {
                        req.flash('errorMessage', 'Message not found');
                        res.redirect('/messages');   
                    }
                });
            } else {
                req.flash('errorMessage', errors[0].msg);
                res.redirect('/messages/reply/' + req.params.messageId);
            }
        });

        this._server.get('/trade/new', [local, auth], function(req, res, next) {
            res.render('index/new-listing', {
                title: 'New Listing'
            });
        });

        this._server.get('/trade/delete/:listingId', [local, auth], function(req, res, next) {
            var queryString = "DELETE FROM listings WHERE listingId = ? AND seller = ?";
            var queryValues = [
                req.params.listingId,
                req.session.account.accountId
            ];

            db_uvoid.query(queryString, queryValues, function(err, result) {
                if(err) {
                    console.log(err);
                }
                req.flash('successMessage', 'Listing has been deleted');
                res.redirect('/trade');
            });
        });

        this._server.post('/trade/new', [local, auth], function(req, res, next) {
            req.assert('listingTitle', 'Please enter a listing title. 5-150 characters').notEmpty().len(5, 150);
            req.assert('listingPrice', 'Please enter a listing price. 0.00 if free').notEmpty();
            req.assert('listingDescription', 'Please enter listing description. 50-1000 characters').notEmpty().len(50, 1000);
            req.sanitize('listingTitle').xss();
            req.sanitize('listingPrice').xss();
            req.sanitize('listingDescription').xss();
            req.sanitize('listingDescription').xss();

            var errors = req.validationErrors();
            if(!errors) {
                if(req.body.listingPrice > 0 && req.session.account.bitcoinAddress.length < 10 || req.body.listingPrice < 0) {
                    req.flash('errorMessage', 'You cannot create non-free listings without a valid receiving bitcoin address');
                    res.redirect('/trade');
                } else {
                    var queryString = "INSERT INTO listings(title, seller, price, description, timestamp, photoUrl, category) VALUES(?, ?, ?, ?, UNIX_TIMESTAMP(), ?, ?)";
                    var queryValues = [
                        req.body.listingTitle,
                        req.session.account.accountId,
                        req.body.listingPrice,
                        req.body.listingDescription,
                        req.body.listingPhoto,
                        req.body.newListingCategory
                    ];
                    db_uvoid.query(queryString, queryValues, function(err, result) {
                        if(err) {
                            console.log(err);
                        }
                        req.flash('successMessage', 'Your listing has been added');
                        res.redirect('/trade');
                    });
                }
            } else {
                req.flash('errorMessage', errors[0].msg);
                res.redirect('/trade/new');
            }
        });

        this._server.get('/settings', [local, auth], function(req, res, next) {
            res.render('index/settings', {
                title: 'Settings'
            });
        });

        this._server.post('/settings', [local, auth], function(req, res, next) {
            if(req.body.newPassword != req.body.confirmedNewPassword) {
                req.flash('errorMessage', 'Passwords do not match');
            } else {
                if(req.body.newPassword !== '') {
                    if(req.body.newPassword.length > 5) {
                        var queryString = "UPDATE accounts SET password = ? WHERE accountId = ?";
                        var queryValues = [
                            common.createSha512Hash(req.body.newPassword),
                            req.session.account.accountId
                        ];
                        db_uvoid.query(queryString, queryValues);
                        var errors = false;
                    } else {
                        var errors = true;
                        req.flash('errorMessage', 'Password must be atleast 5 characters long');
                    }
                }

                if(req.body.bitcoinAddress.length > 10) {
                        var queryString = "UPDATE accounts SET bitcoinAddress = ? WHERE accountId = ?";
                        var queryValues = [
                            req.body.bitcoinAddress,
                            req.session.account.accountId
                        ];
                        db_uvoid.query(queryString, queryValues);
                    }
                    
                if(!errors) {
                    _this.getAcccount(req.session.account.accountId, null, null, function(result) {
                        req.session.account = result;
                        req.flash('successMessage', 'Settings have been successfully saved!');
                        res.redirect('/settings');
                    });
                }
            }
            
        });

        this._server.get('/login', [local], function(req, res, next) {
            if(req.session.account) {
                res.redirect('/');
            }
            res.render('login', {
                title: 'Login'
            });
        });

        this._server.post('/login', [local], function(req, res) {
            req.assert('loginUsername', 'Please enter a username').notEmpty();
            req.assert('loginPassword', 'Please enter a password').notEmpty();
            req.sanitize('loginUsername').xss();
            req.sanitize('loginPassword').xss();

            var errors = req.validationErrors();
            if(!errors) {
                _this.getAcccount(null, req.body.loginUsername, common.createSha512Hash(req.body.loginPassword), function(result) {
                    if (result) {
                        _this.getCategories(function(categories) {
                            req.session.categories = categories;
                            req.session.account = result;
                            res.redirect('/');
                        });
                    } else {
                        req.flash('errorMessage', 'Invalid username/password combination');
                        res.redirect('/login');
                    }
                });
            } else {
                req.flash('errorMessage', errors[0].msg);
                res.redirect('/login');
            }
        });

        this._server.get('/logout', function(req, res) {
            req.session.destroy();
            res.redirect('/');
        });
    },

    getMessage: function(messageId, callback) {
        var queryString = "SELECT * FROM messages WHERE messageId = ?";
        var queryValues = [
            messageId
        ];

        db_uvoid.query(queryString, queryValues, function(err, result) {
            if(err) {
                console.log(err);
            }
            callback(result[0]);
        });
    },

    getAcccount: function(accountId, username, password, callback) {
        if(accountId) {
            var queryString = 'SELECT * FROM accounts WHERE accountId = ?';
            var queryValues = [
                accountId
            ];
        } else {
            var queryString = 'SELECT * FROM accounts WHERE username = ? AND password = ?';
            var queryValues = [
                username,
                password
            ];
        }

        db_uvoid.query(queryString, queryValues, function(err, result) {
            callback(result[0]);
        });
    },

    getListing: function(listingId, callback) {
        var queryString = "SELECT * FROM listings LEFT JOIN accounts ON accounts.accountId = listings.seller WHERE listingId = ?";
        var queryValues = [
            listingId
        ];
        db_uvoid.query(queryString, queryValues, function(err, result) {
            if(err) {
                console.log(err);
            }
            callback(result[0]);
        })
    },

    sendMessage: function(sender, receiver, body, callback) {
        var queryString = "INSERT INTO messages(fromAccount, toAccount, body, status) VALUES(? ,?, ?, 0)";
        var queryValues = [
            sender,
            receiver,
            body
        ];
        db_uvoid.query(queryString, queryValues, function(err, result) {
            if(err) {
                console.log(err);
            }
            callback();
        });
    },

    getCategories: function(callback) {
        var queryString = "SELECT * FROM categories";
        var queryValues = [];
        db_uvoid.query(queryString, queryValues, function(err, results) {
            if(err) {
                console.log(err);
            }
            callback(results);
        })
    },

    getTransaction: function(listingId, buyerId, callback) {
        var queryString = "SELECT * FROM transactions WHERE buyer = ? AND listingId = ?";
        var queryValues = [
            buyerId,
            listingId
        ];
        
        db_uvoid.query(queryString, queryValues, function(err, result) {
            if(err) {
                console.log(err);
            }
            callback(result[0]);
        });
    },

    checkTransaction: function(listing, accountId, cb) {
        var _this = this;
        this.getTransaction(listing.listingId, accountId, function(tx) {
            if(tx) {
                _this._bitcoin.getReceivedByAddress(tx.tempAddress, 1, function(err, received) {
                    if(received >= listing.price) {
                        _this.processTransaction(received, tx, listing, function(res) {
                            cb(res)
                        })
                    } else {
                        cb(false);
                    }
                });
            } else {
                cb(false);
            }
        });
    },

    processTransaction: function(amount, tx, listing, cb) {
        var _this = this;
        var cutAmount  = amount * 0.01;
        var sendAmount = amount - cutAmount;
        _this.getAcccount(listing.seller, null, null, function(seller) {
            _this.completeTransaction(seller, tx.buyer, cutAmount, sendAmount, listing, function() {
                cb(true);
            });
        });
    },

    completeTransaction: function(seller, buyer, cutAmount, sendAmount, listing, cb) {
        var _this = this;
        _this._bitcoin.sendToAddress('1DUpxT6kA6Jk5HEH94M8qskEiwwxZGbKL4', cutAmount, function(err, tx) {
            _this._bitcoin.sendToAddress(seller.bitcoinAddress, sendAmount, function(err2, tx2) {
                var buyMsg  = 'Your listing "' + listing.title + '" has been purchased by ' + buyer.username + '';
                    if(listing.price > 0) {
                        buyMsg += ' - ' + listing.price + 'BTC will be transferred to your seller address shortly';
                    }
                    buyMsg += ' - If the seller includes additional information, you will receive a seperate message';
                var soldMsg = 'This is confirmation that you have purchased the listing "' + listing.title + '" for a total of ' + listing.price + ' BTC';
                _this.transactionNotify(buyMsg, soldMsg, buyer, seller, function(nResult) {
                    cb(true);
                });
            });
        });
    },

    transactionNotify: function(buyMsg, soldMsg, buyer, seller, cb) {
        var _this = this;
        _this.sendMessage(buyer.accountId, seller.accountId, soldMsg, function() {
            _this.sendMessage(seller.accountId, buyer.accountId, buyMsg, function() {
                cb(true);
            });
        });
    }
};

exports.server = Server;
