'use strict';

/**
 * @author    Elmario Husha <elmario@wizardsworkshop.co.uk>
 * @copyright © 2013 Wizards Workshop Limited (08253288), WMXVI
*/

var auth = function(req, res, next) {
    if (!req.session.account) {
        res.redirect('/login');
    } else {
        next();
    }
}
exports.auth = auth;