'use strict';

/**
 * @author    Elmario Husha <elmario@wizardsworkshop.co.uk>
 * @copyright © 2013 Wizards Workshop Limited (08253288), WMXVI
*/

var local = function(req, res, next) {
    res.locals.errorMessage = req.flash('errorMessage');
    res.locals.successMessage = req.flash('successMessage');
    res.locals.session = req.session;
    next();
}
exports.local = local;