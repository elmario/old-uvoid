'use strict';

var config = require('./config').config;
var mysql  = require('mysql');
var crypto = require('crypto');

var Common = {
    createSha512Hash: function(string) {
        return crypto.createHmac('sha512', config.keys.salt).update(string).digest('hex');
    },

    createUsername: function() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 9; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    db: {
        connect: function(name) {
            var data = config.db[name];
            var connection = mysql.createConnection({
                host: data.hostname,
                user: data.username,
                password: data.password,
                database: data.database
            });

            connection.connect(function(err) {
                if(err) {
                    console.log(err);
                }
            });

            connection.on('error', function(err) {
                console.log('MYSQL - re-connecting lost connection: ' + err.stack);
                connection = mysql.createConnection(database.config);
                connection.connect();
            });

            this.connection = connection;
            return this.connection;
        },

        disconnect: function() {
            this.connection.end(function(err) {
                if(err) {
                    console.log(err);
                }
            });
        }

    }
};

exports.common = Common;
