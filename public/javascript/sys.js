'use strict';

$(function () {
    var checkNewMessages = function() {
        $.get('/messages/unreadCount', function(count) {
            $('.messageCount').text(count);
        });
    };

    checkNewMessages();


    $('.saveSettings').click(function() {
        $('#settingsForm').submit();
    });

    var checkInterval = 0;
    $('.buyButton').click(function() {
        var listingId = $(this).attr('data-listingId');
        $('.saleBtcRecipient').text('Loading code...');
        $('.saleBtcAmount').text($(this).attr('data-amount') + ' BTC');
        $('#generatedCode').fadeIn();
        $.get('/trade/start/' + listingId, function(res) {
            $('.saleBtcRecipient').text(res);
            checkInterval = setInterval(function() {
                $.get('/trade/check/' + listingId, function(res) {
                    if(res) {
                        $('#generatedCode').fadeOut(function() {
                            $('#paymentComplete').fadeIn();
                            $('#initMessageId').attr('value', listingId);
                            clearInterval(checkInterval);
                        });
                    }
                 });
            }, 2500);
        }); 
        return false;
    });

    $('.close-dialog-red, .close-dialog-green').click(function() {
      clearInterval(checkInterval);
      $(this).parent().fadeOut();
    });

    $('.deleteButton').click(function() {
      window.location = '/trade/delete/' + $(this).attr('data-listingId');
    });

    setInterval(function() {
        checkNewMessages();
    }, 5000);

    $('#dataTable').dataTable({
      "bJQueryUI": false,
      "iDisplayLength": 5,
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": false,
      "sPaginationType": "full_numbers",
      "bAutoWidth": false,
      "aoColumns": [
              { "sWidth": "50%" },
              { "sWidth": "10%" },
              { "sWidth": "10%"},
              { "sWidth": "20%"},
              { "sWidth": "10%"}
        ],
      "oLanguage": {
        "sSearch": "",
        "sLengthMenu": "<a href='/trade/new' class='small-button'>New Listing</a> <a href='/messages' style='color:#aaa;'>(<span class='messageCount'>0</span> messages)</a><span></span>"
      }
    });

    $('.dataTables_filter input').attr("placeholder", 'search entries');
    $('.dataTables_filter input').addClass('field');
});