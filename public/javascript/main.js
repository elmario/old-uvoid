'use strict';

$(function () {
    setTimeout(function(){
        $('#message').fadeOut(2000);
    }, 2000);

    $('.submitLogin').click(function() {
        $('#loginForm').submit();
    });
});
