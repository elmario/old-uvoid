'use strict';

var config = require('./config').config;
var server = new(require('./server').server);
server.init({
    port: config.client.port
});