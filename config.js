'use strict';

var config = {      
    client: {
        // http://179.43.133.226:54412/
        port: 8080,
        express: {
            viewFolder: 'views',
            viewEngine: 'jade',
            publicFolder: 'public',
            favicon: 'public/assets/images/favicon.ico'
        }
    },
    
    keys: {
        cookie:  '2958492892992',
        salt:    '6829283848472',
        session: '6952819198384'
    },

    db: {
        uvoid: {
            hostname: 'localhost',
            username: '3948444932',
            password: '2e22u346q949e212j3396f34',
            database: 'uvoid'
        },
    }
};

exports.config = config;